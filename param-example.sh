#! /bin/bash

# # Params
# echo "all params: $*"
# echo "number of params: $#"

# #Variable anounce example
# myvar=22

# echo "$myvar"


# Params check for folder type
for param in $*
    do
        if [ -d "$param" ]
        then
            echo "executing scripts in the config folder"
            ls -l "$param"
        fi

        echo $param
    done


# # Calculating program with "q" for exit
# sum=0
# while true
# do
#     read -p "enter a score" score

#     if [ "$score" == "q" ]
#     then
#         break
#     fi

#     sum=$(($sum+$score))
#     echo "total score: $sum"

# done


#Funcions define example
function score_sum {
    sum=0
    while true
    do
        read -p "enter a score" score

        if [ "$score" == "q" ]
        then
            break
        fi

        sum=$(($sum+$score))
        echo "total score: $sum"
    
    done
}

# # Function invoke example
# score_sum


# Function that use parametrs provided in a script
function create_file() {
    file_name=$1
    is_shell_script=$2
    touch $file_name
    echo "file $file_name created"

    if [ "$is_shell_script" = true ]
    then
        chmod u+x $file_name
        echo "added execute permission"
    fi
} 

create_file test.txt
create_file myfile.yaml
create_file myscript.sh true


# Function example
function sum() {
    total=$(($1+$2))
    return $total
}

# result=$(sum 2 10)

sum 2 10
result=$?

echo "sum pf 2 and 10 is $result"